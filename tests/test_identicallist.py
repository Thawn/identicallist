import unittest
import sys
import os
try:
    import identicallist
except ModuleNotFoundError:
    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    import identicallist


class DummyItem:
    def __init__(self):
        self.num = 5

    def inc(self):
        self.num += 1

    def dec(self):
        self.num -= 1

    def mul(self, m):
        self.num *= m

    def ret_add(self, a):
        self.num += a
        return self.num


class TestIdenticalList(unittest.TestCase):
    def setUp(self):
        self.l = identicallist.IdenticalList(DummyItem(), DummyItem())

    def test_list(self):
        self.assertEqual(len(self.l), 2)
        try:
            iter(self.l)
        except TypeError:
            self.fail('{} is not iterable'.format(self.l))

    def test_identical(self):
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertIsInstance(item, DummyItem)

    def test_attr(self):
        self.assertTrue(hasattr(self.l, 'num'))
        self.assertFalse(hasattr(self.l, 'foo'))
        self.assertEqual(self.l.num, 5)
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 5)
        self.l.num = 7
        self.assertEqual(self.l.num, 7)
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 7)

    def test_individual_items(self):
        self.l[1].num = 6
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 5 + i)
        self.l[1].inc()
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 5 + 2 * i)
        self.l.inc()
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 6 + 2 * i)
        self.l.dec()
        self.l.dec()
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 4 + 2 * i)
        ret = self.l.ret_add(2)
        self.assertEqual(ret, 6)
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 6 + 2 * i)

    def test_method(self):
        self.assertTrue(hasattr(self.l, 'inc'))
        self.assertTrue(hasattr(self.l, 'dec'))
        self.assertFalse(hasattr(self.l, 'bar'))
        self.l.mul(2)
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 10)
        self.l.inc()
        self.assertEqual(self.l.num, 11)
        for i, item in enumerate(self.l):
            with self.subTest(i=i):
                self.assertEqual(item.num, 11)
        self.l.dec()
        self.assertIsInstance(item, DummyItem)

    def test_append(self):
        self.l.append(DummyItem())
        self.assertEqual(len(self.l), 3)
        self.assertRaises(TypeError, self.l.append, 5)

    def test_append_wrong_type(self):
        self.assertRaises(TypeError, self.l.append, 6)


if __name__ == '__main__':
    unittest.main()
